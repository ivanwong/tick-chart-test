import techan from 'techan'
var d3 = window.d3;

export default class TickChartRenderer {

	render(el, props, payload) {
		this.destroy()
		this.svg = d3.select(el).append("svg")
			.attr("width", props.width + props.margin.left + props.margin.right)
			.attr("height", props.height + props.margin.top + props.margin.bottom)
		//this.timeParser = d3.time.format("%Y-%m-%dT%H:%M:%S.%L").parse
		this.timeParser = d3.time.format("%Y-%m-%dT%H:%M:%S.%LZ").parse

		//this.el = el
		this.props = props
		this.x = techan.scale.financetime().range([0, props.width])
		this.x2 = techan.scale.financetime().range([0, props.width])
		this.y = d3.scale.linear().range([props.height, 0])
		this.y2 = d3.scale.linear().range([props.height2, 0])
		this.yVolume = d3.scale.linear().range([this.y(0), this.y(0.3)])
		this.brush = d3.svg.brush().on("brushend", this.draw)
		this.tick = techan.plot.tick().xScale(this.x).yScale(this.y)
		this.volume = techan.plot.volume().xScale(this.x).yScale(this.yVolume)
		this.close = techan.plot.close().xScale(this.x2).yScale(this.y2)
		this.xAxis = d3.svg.axis().scale(this.x).orient("bottom")
		this.xAxis2 = d3.svg.axis().scale(this.x2).orient("bottom")
		this.yAxis = d3.svg.axis().scale(this.y).orient("left")
		this.yAxis2 = d3.svg.axis().scale(this.y2).ticks(0).orient("left")
		this.tickAnnotation = techan.plot.axisannotation().axis(this.yAxis).format(d3.format(',.6fs'))
		this.timeAnnotation = techan.plot.axisannotation().axis(this.xAxis)
			.format(d3.time.format('%H:%M:%S.%L'))
			.width(70)
			.translate([0, props.height])

		this.crosshair = techan.plot.crosshair().xScale(this.x).yScale(this.y)
			.xAnnotation(this.timeAnnotation)
			.yAnnotation(this.tickAnnotation)

		// can be removed - code was moved to above
		/*this.svg = d3.select(el).append("svg")
			.attr("width", props.width + props.margin.left + props.margin.right)
			.attr("height", props.height + props.margin.top + props.margin.bottom)*/

		this.focus = this.svg.append("g")
			.attr("class", "focus")
			.attr("transform", "translate(" + props.margin.left + "," + props.margin.top + ")")


		this.focus.append("clipPath")
			.attr("id", "clip")
			.append("rect")
			.attr("x", 0)
			.attr("y", this.y(1))
			.attr("width", props.width)
			.attr("height", this.y(0) - this.y(1))

		this.focus.append("g").attr("class", "spread").attr("clip-path", "url(#clip)")
		this.focus.append("g").attr("class", "tick").attr("clip-path", "url(#clip)")
		this.focus.append("g").attr("class", "x axis").attr("transform", "translate(0," + props.height + ")")
		this.focus.append("g")
			.attr("class", "y axis")
			.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "end")
			.text("Price ($)")

		this.focus.append('g').attr("class", "crosshair").call(this.crosshair)


		this.context = this.svg.append("g")
			.attr("class", "context")
			.attr("transform", "translate(" + props.margin2.left + "," + props.margin2.top + ")")

		this.context.append("g").attr("class", "close")
		this.context.append("g").attr("class", "pane")
		this.context.append("g").attr("class", "x axis").attr("transform", "translate(0," + props.height2 + ")")
		this.context.append("g").attr("class", "y axis").call(this.yAxis2)

		this.accessor = this.tick.accessor()
		this.zoomable = null
		this.zoomable2 = null

		let data = []

		data = this._explain(payload)
		// NOTICE: maybe give performance problems. Need investigations
		data.sort((a, b) => {
			return d3.ascending(this.accessor.d(a), this.accessor.d(b))
		})

		this.x.domain(data.map(this.accessor.d))
		this.x2.domain(this.x.domain())
		this.y.domain(techan.scale.plot.ohlc(data, this.accessor).domain())
		this.y2.domain(this.y.domain())
		this.yVolume.domain(techan.scale.plot.volume(data).domain())

		this.focus.select("g.tick").datum(data)
		this.focus.select("g.spread").datum(data)

		this.context.select("g.close").datum(data).call(this.close)
		this.context.select("g.x.axis").call(this.xAxis2)

		// Associate the brush with the scale and render the brush only AFTER a domain has been applied
		this.zoomable = this.x.zoomable()
		this.zoomable2 = this.x2.zoomable()
		this.brush.x(this.zoomable2)
		this.context.select("g.pane").call(this.brush).selectAll("rect").attr("height", this.props.height2)

		this.draw()

	}

	draw() {
		let tickSelection = this.focus.select("g.tick")
		let data = tickSelection.datum()
		this.zoomable.domain(this.brush.empty() ? this.zoomable2.domain() : this.brush.extent())
		this.y.domain(techan.scale.plot.ohlc(data.slice.apply(data, this.zoomable.domain()), this.tick.accessor()).domain())
		tickSelection.call(this.tick)
		this.focus.select("g.spread").call(this.volume)
		this.focus.select("g.x.axis").call(this.xAxis)
		this.focus.select("g.y.axis").call(this.yAxis)
	}

	_explain(payload) {
		let tmp = payload.map(i => {
			return {
				date: this.timeParser(i.time),
				high: +i.ask,
				low: +i.bid,
				spread: (i.ask - i.bid)/0.0001,
				volume: (i.ask - i.bid)/0.0001,
				open: +i.ask,
				close: +i.bid
			}
		})

		return tmp
	}

	destroy(el = null) {
		if (el == null) {
			d3.select("svg").remove()
			return
		}

		d3.select(el).remove()
	}
}
