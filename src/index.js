import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import IndexPage from './components/pages/IndexPage'
import AboutPage from './components/pages/AboutPage'

import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import './styles/common.styl'
//import 'bootstrap/dist/css/bootstrap.css'

/*import d3 from '../../vendor/d3/d3'
import techan from 'techan'*/

//ReactDOM.render(<App />, document.getElementById('root'));


ReactDOM.render((
	<Router history={browserHistory}>
		<Route path="/" component={App}>
			<IndexRoute component={IndexPage}/>
			<Route path="/about"  component={AboutPage}/>
		</Route>
	</Router>
), document.getElementById('app-root'))


// <Route path="*" component={NoMatch}/>