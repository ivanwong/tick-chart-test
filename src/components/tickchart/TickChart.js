
import React , { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'

import techan from 'techan'
import TickChartRenderer from '../../utils/TickChartRenderer'

require('./style.css')


// http://bl.ocks.org/andredumas/f6b6f776becc34b9c71a
export default class TickChart extends Component {

	constructor(props) {
		super(props)
		this.data = []
		this.tickRenderer = new TickChartRenderer()
		this.socket = window.io();
	}

	componentDidMount() {
		this.socket.emit('init', '')
		let self = this
		this.socket.on('data-chunk', msg => {
			let json = JSON.parse(msg)
			self.data = self.data.concat(json.data)
			// optimization
			if(self.data.length > 5000) {
				self.data = self.data.slice(0, 1000)
			}
			self.tickRenderer.render(ReactDOM.findDOMNode(self), self.props.dimension, self.data)
		})
	}

	componentWillUnmount() {
		this.socket.disconnect()
	}

	render() {
		return (
			<div>
				<div id="chart"></div>
			</div>
		)
	}
}

TickChart.propTypes = {
	dimension: PropTypes.object.isRequired
}

