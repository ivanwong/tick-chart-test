import React, { Component } from 'react';
import SideBar from './sidebar/SideBar'
export default class App extends Component {
  render() {
    return (
      <div>
          <SideBar></SideBar>
          <div className="content-container">
              {this.props.children}
          </div>
      </div>
    );
  }
}
