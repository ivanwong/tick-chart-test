import React, { Component } from 'react'
import TickChart from '../tickchart/TickChart'

export default class IndexPage extends Component {
	render() {
		let dim = {
			margin: {
				top: 20, right: 20, bottom: 100, left: 55
			},
			margin2: {
				top: 420, right: 20, bottom: 20, left: 55
			},
			width: 960 - 55 - 20,
			height: 500 - 20 - 100,
			height2: 500 - 420 - 20
		}
		return (
			<div>
				<TickChart dimension={dim}></TickChart>
			</div>
		)
	}
}

