import React, { Component } from 'react'
import { Link } from 'react-router'
require('./style.styl')

export default class SideBar extends Component {
	render() {
		return (
			<aside>
				<h1>sidebar</h1>

				<h4>
					<Link to="/">Tick chart</Link>
				</h4>
				<h4>
					<Link to="/about">About</Link>
				</h4>
			</aside>
		)
	}
}


