var path = require('path');
var webpack = require('webpack');

module.exports = {
	devtool: 'eval',
	entry: [
		'webpack-dev-server/client?http://localhost:3000',
		'webpack/hot/only-dev-server',
		'./src/index'
	],
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: '/static/'
	},
	resolve: {
		extensions: ['', '.js']
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	],
	module: {
		loaders: [
			{
				test: /\.js$/,
				loaders: ['react-hot', 'babel'],
				include: path.join(__dirname, 'src'),
				exclude: 'node_modules'
			},
			{
				test: /\.css$/,
				exclude: ['/node_modules/'],
				loader: "style-loader!css-loader"
			},
			{
				test: /\.styl$/,
				loader: 'style-loader!css-loader!stylus-loader'
			},
			{
				test: /\.(png|jpg)$/,
				loader: 'file-loader?name=[sha1:hash:base26:16].[ext]'
			},
			{
				test: /\.(csv)$/,
				loader: 'file-loader?name=[sha1:hash:base26:16].[ext]'
			},
			{
				test: /\.woff([\?]?.*)$/,
				loader: "file-loader?mimetype=application/font-woff&&name=[sha1:hash:base26:16].[ext]"
			},
			{
				test: /\.woff2([\?]?.*)$/,
				loader: "file-loader?mimetype=application/font-woff&name=[sha1:hash:base26:16].[ext]"
			},
			{
				test: /\.ttf([\?]?.*)$/,
				loader: "file-loader?mimetype=application/octet-stream&name=[sha1:hash:base26:16].[ext]"
			},
			{
				test: /\.otf([\?]?.*)$/,
				loader: "file-loader?mimetype=application/x-font-ttf&name=[sha1:hash:base26:16].[ext]"
			},
			{
				test: /\.eot([\?]?.*)$/,
				loader: "file-loader?name=[sha1:hash:base26:16].[ext]"
			},
			{
				test: /\.svg([\?]?.*)$/,
				loader: "file-loader?mimetype=image/svg+xml&name=[sha1:hash:base26:16].[ext]"
			}
		]
	}
};
