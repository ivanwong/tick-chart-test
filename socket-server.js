
var app = require('express')();
var express = require('express')
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('./'));

// comment this block if nginx will used
app.get('/', function(req, res){
	res.sendfile('index.html');
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});


io.on('connection', function(socket){
	socket.on('init', function(msg){
		var initial = getChunks(500)
		socket.emit('data-chunk', JSON.stringify({data: initial}));
		setInterval(function() {
			initial = getChunks(4)
			socket.emit('data-chunk', JSON.stringify({data: initial}));
		}, 1000)
	});
});


function getChunks(packetCount) {
	var data = []
	for (var i=0; i <= packetCount; i++) {
		var d = new Date()
		d = new Date(d.setMinutes(d.getMinutes()+i))
		data.push({
			time: d.toISOString(),
			bid: getRandomPrice(0.71111, 0.72999, 5),
			ask: getRandomPrice(0.71111, 0.72999, 5)
		})

	}

	return data;

}


// utils func
function getRandomPrice(min, max, fix) {
	var result =  Math.random() * (max - min) + min;
	return result.toFixed(fix)
}
